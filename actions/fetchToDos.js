import { FETCH_TODOS_SUCCESS, FETCH_TODOS_FAILURE, FETCHING_TODOS } from '../constants';
import axios from 'axios';

export function runRequest() {

    return (dispatch) => {
        dispatch(getToDos())
        axios.get('https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0')
            .then(function (response) {
                // transform array from Fetch into array of Rows so we could output data for two picture in a row 
                let outputArray = new Array(Math.ceil(response.data.length / 2)).fill(null).map(() => Array(2));
                let mapObject   = new Map;
                let row = 0;
                response.data.forEach((element, index) => {
                    outputArray[row][index % 2] = element;
                    if (index % 2 === 1) {
                        row++;
                    }

                    mapObject.set(element.id, {
                        'userName': element.user.name,
                        'url': element.urls.raw,
                        'description': element.alt_description
                    });
                });

                return (dispatch(getToDosSuccess({"outputArray":outputArray, "mapObject":mapObject})))
            })
            .catch(err => dispatch(getToDosFailure(err)))
    }
}


export function getToDos() {
    return {
        type: FETCHING_TODOS
    }
}

export function getToDosSuccess(data) {

    return {
        type: FETCH_TODOS_SUCCESS,
        data
    }
}

export function getToDosFailure(data) {
    return {
        type: FETCH_TODOS_FAILURE,
        data
    }
}