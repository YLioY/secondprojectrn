import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

export const MyNavigation = props => {
    return (
        <View style={styles.navbar}>
            <Text ellipsizeMode='tail' numberOfLines={1}> {props.Title} </Text>
        </View>
    )

}

const styles = StyleSheet.create({
    navbar: {
        top: 0,
        left: 0,
        height:50,
        alignItems: "center",
        justifyContent: "flex-end",
        backgroundColor: '#3949ab',
        paddingBottom: 10 
    },

})