import React from 'react';
import {
    View,

    StyleSheet,
    Image,
    Text,
    TouchableHighlight
} from 'react-native';


export default function ImageDisplay(props) {
    
    params = props.params;

    const onPressImage =() => {
        props.changePageFunction(props.pictureID)   
    }

    return (
        <View style={styles.pictureView}>
            <View >
                <Text numberOfLines={1}  ellipsizeMode='tail' style={styles.textContainer} > {params.user.name} </Text>
            </View>

            <TouchableHighlight  onPress={onPressImage}> 
                <Image
                    key={'img' + params.id}
                    style={styles.picture}
                    source={{ uri: params.urls.raw }}
                />
            </TouchableHighlight>

            <View   style={{ flexDirection: 'row' }}  >
                <Text style={{ flex: 1 }} > {params.alt_description} </Text>
            </View>
        </View>
    )
}



const styles = StyleSheet.create({
    picture: {
        flex: 1,
        paddingBottom: 10,
        aspectRatio: 1,
    },
    pictureView: {
        flex: 1,
        flexDirection: 'column',
        padding: 1,
        borderWidth: 1
    },
    textContainer: {
        textAlign:'center',
    }

});