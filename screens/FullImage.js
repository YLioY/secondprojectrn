import React from 'react'
import { MyNavigation } from '../modules/Navigation'
import { StyleSheet, Text, View, Image, TouchableHighlight } from 'react-native'


export default function FullImage(props) {
    console.log('props', props);

    if (props.route.params === undefined) {
        return (
            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                <Text> You have had to choose any picture before entering here</Text>
            </View>
        )
    }


    let imageObject = props.route.params;

    return (
        <View style={styles.pictureView}>
            <MyNavigation Title={imageObject.userName} />

            <TouchableHighlight style={styles.picture} onPress={() => props.navigation.goBack()} >
                <Image
                    style={styles.picture}

                    source={{ uri: imageObject.url }}
                />
            </TouchableHighlight>

            <Text> {imageObject.description} </Text>
        </View>
    )
}


const styles = StyleSheet.create({
    picture: {
        flex: 1,
        aspectRatio: 1,

    },
    pictureView: {
        flex: 1,
        flexDirection: 'column',
        padding: 1,
        borderWidth: 1


    }
});