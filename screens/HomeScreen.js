import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    ScrollView,
    ActivityIndicator
} from 'react-native';
import ImageDisplay from '../modules/ImageDisplay';
import { connect } from 'react-redux';
import * as fetchTodos from '../actions/fetchToDos';

import { MyNavigation } from '../modules/Navigation'

class HomeScreen extends Component {

    componentDidMount() {

        this.props.runRequest();

    }

    render() {

        const { fetchResult, mapObject, isFetching, error } = this.props.todos

        if (isFetching) {
            return (
                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                    <ActivityIndicator size={'large'} />
                </View>
            )
        } else {

            if (error) {
                console.log('after fetch error', JSON.stringify(fetchResult).substr(0, 200));
            }

            const changePage = (objectId) => {

                this.props.navigation.navigate('FullImage', mapObject.get(objectId))
            }


            return (
                <View style={styles.flexColum}>
                    <MyNavigation Title='List of images' />

                    <ScrollView style={styles.flexColum} >

                        {
                            fetchResult.map((rowArray, index) => {

                                return <View key={"row" + index.toString()} style={{ flex: 2, flexDirection: 'row' }} >
                                    {
                                        rowArray.map((currentValue,rowIndex) => {
                                            return <ImageDisplay key ={'ImageDisplay'+index.toString() + rowIndex.toString()} params={currentValue} changePageFunction={changePage} pictureID={currentValue.id}/> 
                                        })
                                    }
                                </View>
                            })
                        }
                    </ScrollView>
                </View>

            )
        }

    }

}





function mapStateToProps(state) {

    return {
        todos: state.todos
    }
}

function mapDispatchToProps(dispatch) {

    return {
        runRequest: () => dispatch(fetchTodos.runRequest()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)



const styles = StyleSheet.create({

    flexColum:{
        flex: 1, 
        flexDirection: 'column' 
    }

});