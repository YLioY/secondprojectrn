import { FETCH_TODOS_SUCCESS, FETCH_TODOS_FAILURE, FETCHING_TODOS } from '../constants';

const initialState = {
    fetchResult: [],
    mapObject: new Map,
    isFetching: false,
    error: false
}

export default function todosReducer(state = initialState, action) {

    switch(action.type) {
        case FETCHING_TODOS:
            return {
                ...state,
                isFetching: true
            }
        case FETCH_TODOS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                fetchResult: action.data.outputArray,
                mapObject: action.data.mapObject 
            }
        case FETCH_TODOS_FAILURE:
            return {
                ...state,
                isFetching: false,
                error: true,
                fetchResult: action.data 
            }
        default:
            return state
    }
}