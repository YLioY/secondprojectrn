import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux'
import configureStore from './configureStore';

import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';


import HomeScreen from './screens/HomeScreen';
import FullImage from './screens/FullImage';

const Tab = createBottomTabNavigator();

const store = configureStore()

export default function App() {
  //console.clear();
  return (
    <Provider store={store}>
      <NavigationContainer >
        <Tab.Navigator tabBarOptions={{
          labelStyle: {
            fontSize: 15
          }

        }}>
          <Tab.Screen name="Home" component={HomeScreen}
            options={{
              tabBarLabel: 'Home',
              tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="home" color={color} size={size} />
              ),
            }}
          />
          <Tab.Screen name="FullImage" component={FullImage}
            options={{
              tabBarLabel: 'Full Image',
              tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="google-photos" color={color} size={size} />
              ),
            }}
          />
        </Tab.Navigator>
      </NavigationContainer>
    </Provider>
  )
}